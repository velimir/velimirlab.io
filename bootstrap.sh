#!/usr/bin/env bash

set -e
set -o pipefail

apt-get update
apt-get --yes install \
        git \
        build-essential \
        python \
        python-dev \
        python-pip \
        libjpeg-dev \
        libjpeg-progs \
        optipng

git submodule init
git submodule update --init --recursive
pip install -r requirements.txt
