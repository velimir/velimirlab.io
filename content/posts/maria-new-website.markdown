---
title: Maria's new website/blog
date: '2017-05-21 18:31:01'
category: news
tags: dev, web
image: images/gentleteapot-responsive-preview.png
summary: Maria has got a new website/blog, all her posts were moved.
---

Hi everyone!

I'm glad to announce, that Maria now has a new personal [website/blog](https://gentleteapot.com)!
All her posts were moved and from now on old links redirects to a new address - [https://gentleteapot.com](https://gentleteapot.com).

Everyone is more than welcome to check it out and leave some feedback (we'll appreciate it for sure).
