---
title: Update DNS records for your home server
date: '2015-12-07 23:37:19'
slug: update-dns-records
category: development
tags: programming, python
summary: script to update your DNS records by cron job
---

Here's a plain and simple script to update your DNS records, that you could add to your cron job so that your home server will be always accessible from outside.

Just place in `/etc/cron.hourly` and add executable flag to the file:

`chmod a+x /etc/cron.hourly/update-dns`

<script src="https://gist.github.com/velimir0xff/d38a38345d18b4f52bea.js"></script>
