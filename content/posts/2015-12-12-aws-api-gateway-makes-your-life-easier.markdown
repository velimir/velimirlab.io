---
title: AWS API Gateway, could it make your life easier?
date: '2015-12-12 20:25:26'
slug: aws-api-gateway-makes-your-life-easier
category: development
tags: programming, AWS
image: images/aws-api-gateway-header.jpg
summary: first view on AWS API Gateway
---

Amazon [released](https://aws.amazon.com/releasenotes/Amazon-API-Gateway/5820792193066785) a new cloud product AWS API Gateway, which is a service that could potentially make your life (as a developer) easier. The whole idea behind the service is described on a [front page](https://aws.amazon.com/api-gateway/):

> Amazon API Gateway is a fully managed service that makes it easy for developers to create, publish, maintain, monitor, and secure APIs at any scale. With a few clicks in the AWS Management Console, you can create an API that acts as a “front door” for applications to access data, business logic, or functionality from your back-end services, such as workloads running on Amazon Elastic Compute Cloud (Amazon EC2), code running on AWS Lambda, or any Web application.

I decided to give it a try. Let me start with a brief, 1000 miles view, concept of AWS API Gateway.
Just imagine that you have a service on a backend that serves any data via a HTTP REST interface (I might be wrong but it seemed to me that the type range of the data are not limited only to JSON format). One day you decided to expose your service and make it publicly available. Now you have to option:

* Build your own public API proxy, which will be responsible for
  * authentication, authorization and accounting (AAA)
  * throttling
  * internal relation object mapping (if it's necessary. It could be necessary, just for instance, if your service exposes to much data that should be narrowed only to a required subset)
  * whatever you want
* Use AWS API Gateway service

The first option is a flexible and powerful idea, which at the same time shouldn't take too much time to implement it, though there're some shortcomings that we'd better to avoid:

* That's another one service which will be written by you and most likely it will contain bugs (just admit it)
* For each resource that you want to expose there'll be a scheme - how to map "original" data to a public version.
* You'll probably want to define a set of queries available to clients.
* Each change on the backend should be reflected on the API side, which means additional efforts on API service.

Gateway could help you to avoid that headache.
But only if your service is relatively simple and strictly RESTfull.
While I was trying to create such a lightweight proxy for backend (BTW, I used only UI to make a proof of a concept, though once it's created you can use one of [AWS tools](https://github.com/awslabs/aws-apigateway-importer) that allow you to upload and deploy your API according to API modeling languages), I faced with several things that don't allow me to leverage  AWS service without pain:

* there's no builtin way to deal with authentication that already implemented on your side (I assume you have already pre-populated user database permissions and rights). You could either proxy request to the backend or call a lambda function, but there's no way to pipe them all together.
* request field names are strictly limited to alpha-numerical subset (for example: `obj.name` in request `/api/user?obj.name=ivan` is invalid and you can't pass those queries to the backend because it has a dot)
* there's no convenient way to use non RESTfull paths (like: `/api/user/[userId]/name`, where `userId` is optional). You still can use it, though you will need to duplicate code in both specifications: `/api/user/{userId}/name` and `/api/user/name`. I guess it's relatively rear case, but it shows that everything should be strictly RESTfull.

After a while I realized that AWS API Gateway brings me even more troubles than a standalone API proxy. The better option for me would be a library which would give me an object/query mapping and one of builtin throttling solutions that hapxory/nginx has.

If you have a simple RESTfull backend, which is not yet publicly  exposed and you don't need to support backward compatibility with an old API that you already have - API Gateway is a service that I'd definitely give it a try.

<small>Please if you think I've missed something, let me know. </small>

