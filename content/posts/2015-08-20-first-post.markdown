---
title: Our office
date: '2015-08-20 10:29:01'
slug: first-post
category: work
tags: office, work
image: images/alertlogic-office-5th-floor.jpg
summary: My first post about our fancy office
---

I know that it's my first post and it should be about something really interesting and exciting, but I spend a lot of time on work, so that's kind of one of thing that I'm happy to share.

That's about how amazing our office is :)

[Our office](http://officesnapshots.com/2015/07/10/alert-logic-cardiff-offices)
