---
title: snapaste: snapping pastebin
date: '2016-02-07 20:26:44'
slug: snapaste-snaping-pastebin
category: development
tags: programming, erlang, docker
image: images/trilbi-snapaste-header.jpg
summary: A snapping pastebin implementation. Whatever text data posted to it can only be accessed once.
---

tl;dr: **[snapaste](https://github.com/kstarikov/snapaste)** and its **[dockerized version](https://github.com/velimir0xff/snapaste-docker)**

> # Snapaste
> A snapping pastebin implementation. Whatever text data posted to it can only be accessed once.

### problem
I have a comparatively tangled setup for my pet project's development environment. I use my laptop as a thin client to Linux server. That Linux server runs several lxc containers plus docker containers. Each lxc container has installed emacs which I use from my laptop (X11 forwarding is disabled, actually there's no X-Windows at all). For some developers, I guess, that's a regular setup: run their lovely editors in a plain console mode on a remote machine, which is accessible through some _bastion/jump box_.

This configuration has a lot of disadvantages and one of them is local (laptop) clipboard buffer is not connected to editor. It doesn't sound like a problem (and I think this's not a problem at all for some people), but sometimes I need to copy and paste something (more than 2 screens of text) from emacs, which is running under tmux, to browser or another laptop application. Just try it and you'll feel my pain.

### solution

There're several solutions for this problem:

* run a daemon which knows how to work with your clipboard and proxy all requests when you connect to a remote server. That's not my idea, I've seen it on [github](https://github.com/joddie/nclip.el).
* proxy X-Windows copy/paste. I recon, there's a way to make it work, but it's not suitable for me since I don't even have X servers on remote machines.
* copy/paste to some web server and then fetch result from any other place. This is like a pastebin, but with a command line interface. One of the best examples: http://ix.io. Though that option seems like the best one so far (as to my mind), it has a fatal flaw: it's ~~not invented here~~ not open-sourced.

#### snapaste

One day I discussed the problem with my colleagues and we came up with an idea: snapping pastebin. One of my counterparts decided to implement it and here it is: **[snapaste](https://github.com/kstarikov/snapaste)**

I wrote a really plain and simple **[dockerized version](https://github.com/velimir0xff/snapaste-docker)** of that application.

Now I can use my own open-sourced and "secure" service to copy and paste text documents using emacs from any server, which has `curl`, `httpie` or any other tool to make HTTP requests (there's probably a way to do it just using emacs libraries).

All documentation is updated as of this writing and there's no need to write how to use it, but just to make the picture full: here're install and usage examples.

##### Installation

If you already have a certificate for your domain then you can omit this step (just make sure that you specify the correct path to keys later).

###### Generate ssl certificates

<script src="https://gist.github.com/velimir0xff/023e11bea8f7e805a359.js"></script>

###### Configure application

<script src="https://gist.github.com/velimir0xff/e11c1a9af37e3f505db0.js"></script>

###### Run container

<script src="https://gist.github.com/velimir0xff/9b06245ff1df44478496.js"></script>

Repository has [documentation](https://github.com/velimir0xff/snapaste-docker#build-and-use-your-own-image) how to build and use your own image.

##### Example usage

<script src="https://gist.github.com/velimir0xff/5375eb7993734836eb77.js"></script>

