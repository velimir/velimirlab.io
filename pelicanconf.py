#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals

import functools

AUTHOR = u'velimir'

AUTHORS = {
    'velimir': {
        'picture': 'images/velimir-avatar.png',
        'first_name': 'Grigory',
        'last_name': 'Starinkin',
        'gender': 'male',
        'username': AUTHOR,
        'birth_date': '1989-08-01',
        'email': 'starinkin@gmail.com',
        'height': '175 cm',
        'bio': 'Software developer. Enthusiast.',
        'location': 'London, UK',
        'og': 'https://www.facebook.com/starinkings',
        'twitter': '@velimir0xff'
    }
}

SITENAME = u'yellow sunflower'
TAG_LINE = u'what we do and what we love.'
SITEURL = u'http://localhost:8000'
# [Casper] Site Description
SITE_DESCRIPTION = 'Velimir (Grigory Starinkin) personal blog.'
# [Casper] Site Logo to use in top left corner
SITE_LOGO = None

PUBLISHER = {
    'name': SITENAME,
    'logo': u'images/logo.png'
}

PATH = 'content'

TIMEZONE = 'Europe/London'

# [Casper] Header images
DEFAULT_HEADER_IMAGE = u'images/homepage.jpg'
ARCHIVE_HEADER_IMAGE = None


# Feed generation is usually not desired when developing
FEED_RSS = 'feed.xml'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
SOCIAL = None

SUMMARY_MAX_LENGTH = 20
DEFAULT_PAGINATION = 20

STATIC_PATHS = [
    'images',
    'images/favicons/favicon.ico',
    'extra/robots.txt',
    'redirect'
]
EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    'images/favicons/favicon.ico': {'path': 'favicon.ico'},
    'redirect/hot-air-balloon.html': {'path': 'hot-air-balloon.html'},
    'redirect/london-natural-history-museum.html': {'path': 'london-natural-history-museum.html'},
    'redirect/science-museum.html': {'path': 'science-museum.html'},
    'redirect/the-dyffryn-gardens.html': {'path': 'the-dyffryn-gardens.html'}
}

TEMPLATE_PAGES = {'extra/manifest.json': 'manifest.json'}

ARTICLE_EXCLUDES = ['redirect']

PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'

TWITTER_USERNAME = 'velimir0xff'

DISQUS_SITENAME = 'yellowsunflower'
FACEBOOK_APP_ID = '292320521162710'

TYPOGRIFY=True

THEME = 'themes/Casper'
PLUGIN_PATHS = ['plugins']
PLUGINS = [
    'authors',
    'image_process',
    'neighbors',
    'optimize_images',
    'sitemap',
    'social-meta'
]

# image_process plugin
def article_background_selector(tag):
    classes = tag.attrs.get('class', [])
    return ((tag.name == 'header' and ('main-header' in classes and
                                       'no-cover' not in classes))
            or
            (tag.name == 'a' and ('read-next-story' in classes and
                                  'no-cover' not in classes)))

def thumbnail(image, side_size):
    image.thumbnail((side_size, side_size))
    return image

IMAGE_PROCESS_FORCE = True
IMAGE_PROCESS = {
    'page-header': {
        'type': 'image',
        'ops': [functools.partial(thumbnail, side_size=1280)],
        'selector': article_background_selector,
        'src': {
            'attribute': 'style',
            'path-regex': "background-image: url\('(.*)'\).*"
        },
        'dst': {
            'attribute': 'style',
            'format': "background-image: url('{}')"
        }
    }
}

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.7,
        'pages': 0.6,
        'indexes': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    },
    'exclude': ['manifest.json']
}
