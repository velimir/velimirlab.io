FROM ubuntu:latest

RUN apt-get update && \
    apt-get --yes install \
        git \
        build-essential \
        python \
        python-dev \
        python-pip \
        libjpeg-dev \
        libjpeg-progs \
        optipng

VOLUME /src
WORKDIR /src

CMD git submodule init && \
    git submodule update --init --recursive && \
    pip install -r requirements.txt && \
    make html
